@extends("templates.default.layout.container")
@section("title","|")
@section("meta")
  <meta property="og:url"           content="" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="" />
  <meta property="og:description"   content="  " />
  <meta property="og:image"         content="" />
@stop
@section("container")


<!-- Content -->
<section id="portfolio">
        <article>
            <div class="container">
                <div class="row"> 
                 <header id="header" class="">
                    <div class="title-page">
                      <h1>Portfólio</h1>
                      <small>no momento é isso =)</small>
                    </div>  
                 </header><!-- /header -->
      
                 <section class="portfolio">
                     <ul class="portfolio">
                        @foreach($project as $projects)
                       <!-- items -->
                       <li class="p-item">
                              <div class="view">
                              <h2>{{ $projects->title}}</h2>
                                  <div class="button-view">
                                    <a href="{{ route("project.view",[$projects->id,kebab_case($projects->title)])}}" title="{{ $projects->title}}" class="item-view" > Visualizar projeto</a>
                                  </div>  
                              </div>
                             <div class="front-p">
                                 <a href="{{ route("project.view",[$projects->id,kebab_case($projects->title)])}}" title="{{ $projects->title}}"><img src="{{ asset("uploads/{$projects->thumb}")}}" alt="{{ $projects->title}}"></a>
                             </div>        
                       </li>
                       <!-- end -->
                       @endforeach
                       
                     </ul>
                 </section>
                 
                </div>     
            </div>
        </article>
</section>

<style>
    
.view{
	position:absolute;
	opacity: 0;
	background:;
	width:450px;
	height:450px;
	text-shadow: 2px  #000;
	background:rgba(255,255,255,0.9);
	transition: all 0.3s ease-in;
	text-align: center;
	box-sizing: border-box;
	padding-top: 150px;
	
}
.view:hover{
	opacity: 1;
}
.item-a{
	color:#545f75;
	text-transform: uppercase;
}
.button-view{
	margin-top:50px;
	margin-bottom: 130px
}
.item-view{
	height:50px;
	background:#545f75;
	padding:15px 30px 15px 30px;
	color:#fff;
	border-radius: 2px;
	transition: all 0.3s ease-in;
}
.item-view:hover{
	-webkit-box-shadow: 0px 0px 22px -10px rgba(0,0,0,1);
	-moz-box-shadow: 0px 0px 22px -10px rgba(0,0,0,1);
	box-shadow: 0px 0px 22px -10px rgba(0,0,0,1);
}
</style>

@stop

@push("scripts")
<link rel="stylesheet" href="{{ asset("assets/css/simple-grid.min.css") }}">
@endpush