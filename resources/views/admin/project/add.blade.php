@extends('adminlte::page')

@section('title', 'Adicionar Post')

@section('content_header')
    <h1>Adicionar um post</h1>
@stop

@section('content')
<div class="col-md-8">
    <div class="box box-primary">
       
        <div class="box-header with-border">
            <h3 class="box-title">Adicionar um Projeto</h3>
        </div>
       <div class="col-md-12">
            <div class="box-body">
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Aviso!</h4>
                        {{ session('status') }}
                    </div>
                    
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
       </div>
        <!-- /.box-header -->
        <!-- form start -->
    <form role="form" action="{{ route("admin.project.store") }}" method="post" enctype="multipart/form-data">
             <!-- /.box-body -->
             {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label>Titulo</label>
                    <input type="text" name="title" class="form-control" placeholder="Titulo" value="{!! $posts->title or old("title") !!}">
                </div>
                
                <div class="form-group">
                

              </div>
                <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                    <label>Conteudo</label>
                    <textarea id="some-textarea" name="content" class="form-control" placeholder="" rows="20" value="">{!! $posts->content or old("content") !!}</textarea>
                </div>
                
                <div class="box-footer">
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="submit" class="btn btn-success">Adicionar</button>
                </div>
            </div>
        
    </div>
</div>

<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Thumb</h3>
        </div>
        <div class="box-body">       
               
               
               <input type="file" name="thumb" id="">        
        </div>
    </div>
   
</div>

 </form>


@stop
@push("css")
<link rel="stylesheet" href="{{ asset("vendor/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}">
<link rel="stylesheet" href="{{ asset("vendor/adminlte/plugins/iCheck/square/blue.css")}}">
<link rel="stylesheet" href="{{ asset("vendor/adminlte/plugins/iCheck/all.css")}}">
@endpush
@push("js")
 <script src="{{ asset("vendor/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>
 <script src="{{ asset("vendor/adminlte/plugins/iCheck/icheck.min.js")}}"></script>
 <script>
  $('#some-textarea').wysihtml5();</script>
 
@endpush

