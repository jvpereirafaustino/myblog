<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/blog', 'HomeController@index')->name('blog');
Route::get('/category/{category}', 'HomeController@index')->name('category');
Route::get('/contact', 'ContactController@index')->name('contact');
Route::get('/project', 'ProjectController@index')->name('project');
Route::get('/project/view/{id}-{title?}', 'ProjectController@show')->name('project.view');
Route::get('/about', 'AboutController@index')->name('about');
Route::post('/contact', 'ContactController@store')->name('contact.send');
Route::get('/post/{id}-{title?}', 'Admin\\PostController@show')->name('view');

Route::namespace('Admin')->prefix('admin')->group(function () {
    Route::get('/login', 'adminAuth@index')->name('admin.auth');
    Route::post('/login', 'adminAuth@login')->name('admin.login');
    Route::middleware("auth")->group( function (){
        Route::get('/', 'HomeController@index')->name('admin.home');
        Route::get('/blog', 'PostController@index')->name('admin.post');
        Route::get('/settings', 'SettingsController@index')->name('admin.setting');
        //profile
        Route::get('/profile', 'ProfileController@index')->name('admin.profile');
        Route::post('/profile', 'ProfileController@update')->name('admin.profile.update');
        //Adicionar
        Route::get('/blog/adicionar', 'PostController@create')->name('admin.post.add');
        Route::post('/blog/adicionar', 'PostController@store')->name('admin.post.post');
        //Editar
        Route::get('/blog/edit/{id}', 'PostController@edit')->name('admin.post.edit');
        Route::post('/blog/edit/{id}', 'PostController@update')->name('admin.post.update');
        //Delete
        Route::get('/blog/delete/{id}', 'PostController@destroy')->name('admin.post.del');
        Route::post('/blog/category/new', 'CategoryController@store')->name('admin.cat.new');
        //Manager
        Route::get('/user', 'UserController@index')->name('admin.user');
        Route::get('/user/add', 'UserController@create')->name('admin.user.add');
        //Project
         Route::get('/project', 'ProjectController@index')->name('admin.project');
         Route::post('/project/add', 'ProjectController@store')->name('admin.project.store');
         Route::get('/project/add', 'ProjectController@create')->name('admin.project.add');
        


    });
});
// Auth

  // Authentication Routes...
  Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
  Route::post('login', 'Auth\LoginController@login');
  Route::post('logout', 'Auth\LoginController@logout')->name('logout');

  //   Registration Routes...
  //   Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
  //   Route::post('register', 'Auth\RegisterController@register');

  // Password Reset Routes...
  Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
  Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
  Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
  Route::post('password/reset', 'Auth\ResetPasswordController@reset');