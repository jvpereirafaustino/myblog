<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Project;

class ProjectController extends Controller
{
    public $project;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }
    public function index()
    {
        //
        $project = $this->project->paginate(10);
        return view("admin.project.manager",compact("project"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        return view("admin.project.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'content' => 'required|min:10',
            'thumb' => 'required|file|image|mimes:jpeg,jpg,png|max:2000|dimensions:min_width=450,min_height=450,max_width=450,max_height=450',

        ]);

        if ($request->hasFile('thumb') && $request->file('thumb')->isValid()) {

            $name = uniqid(str_random(10));


            $path = $request->file('thumb')->storePubliclyAs(
                'project', $name . "." . $request->file("thumb")->extension(), "public"
            );

            $project = $this->project->create([
                "title" => $request->get("title"),
                "content" => $request->get("content"),
                "thumb" => $path,
                "status" => 1,
                "autor" => auth()->user()->name

            ]);
        }
        if($project){

            return redirect()->route("admin.project.add")->with('status', 'Adicionado com sucesso!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
